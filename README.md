# Introduction
This is a golang 
# Running
First you need to have a few things installed. 
* Terraform version 0.11.14. I recommend using `tfswitch` to change versions to this version.
* Golang 1.12

To run first build the golang project with `go build`
## Assumptions
This requires the following things
* Aws access
* ssh_key

This will default to using the credentials in `~/.aws/credentials`. 
Other ways to specify is with the env variable `AWS_PROFILE` which uses above file or with `AWS_ACCESS_KEY` and `AWS_SECRET_ACCESS_KEY`.

It also uses a public ssh_key. It uses `~/.ssh/id_rsa.pub` or the path specified in `SSH_PUBLIC_KEY_PATH`
    
# Parts of project
`tf` folder contains all the important information 
# Future changes
These didn't make the cut of a 3 hour project:
* Add ability to run in circleci and/or gitlab
* Run in docker container for portability/versioning of terraform
* upgrade to terraform version 0.12
* use workspaces along with terraform cloud
* Add ansible to run after ec2 instance is accessible instead of runcmd.
* Add ability to launch docker images after setting up image
* Use production rails
* ALB with HTTPS in front
