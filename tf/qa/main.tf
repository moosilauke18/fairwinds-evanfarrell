resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "${var.ssh_public_key}"
}

module "instance" {
  source         = "../modules/tech_challenge"
  key_name       = "${aws_key_pair.deployer.key_name}"
  ssh_public_key = "${var.ssh_public_key}"
  environment    = "${var.environment}"
  my_ip          = "${var.my_ip}"
}

output "instance_public_ip" {
  value = "${module.instance.instance_public_ip}"
}
