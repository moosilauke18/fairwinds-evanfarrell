variable "ssh_public_key" {}

variable "environment" {
  default = "qa"
}

variable "my_ip" {}
