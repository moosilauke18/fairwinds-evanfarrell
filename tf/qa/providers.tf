provider "aws" {
  version = "~> 1.0"
  region  = "ap-southeast-2"
}

provider "template" {
  version = "~> 1.0"
}
