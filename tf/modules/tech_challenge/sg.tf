resource "aws_security_group" "evanfarrell" {
  name        = "evanfarrell"
  description = "Allow access to instance"
  vpc_id      = "${aws_vpc.main.id}"
  tags        = "${local.common_tags}"
}

resource "aws_security_group_rule" "evanfarrell" {
  type              = "egress"
  protocol          = "-1"
  from_port         = 0
  to_port           = 0
  security_group_id = "${aws_security_group.evanfarrell.id}"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "evanfarrell-1" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["${var.my_ip}/32"]
  security_group_id = "${aws_security_group.evanfarrell.id}"
}

resource "aws_security_group_rule" "evanfarrell-2" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["${var.my_ip}/32"]
  security_group_id = "${aws_security_group.evanfarrell.id}"
}
