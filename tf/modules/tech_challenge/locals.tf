locals {
  common_tags = {
    environment    = "${var.environment}"
    terraform_repo = "github.com/moosilauke18/fairwinds-evanfarrell"
  }
}
