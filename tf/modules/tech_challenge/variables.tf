variable "ssh_public_key" {}
variable "key_name" {}

variable "instance_size" {
  default = "t2.medium"
}

variable "environment" {}
variable "my_ip" {}
