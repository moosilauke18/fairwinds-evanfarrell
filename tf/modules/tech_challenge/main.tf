data "template_file" "deploy_user" {
  template = "${file("${path.module}/files/cloud-init/deploy_user.cfg")}"

  vars {
    SSH_KEY = "${var.ssh_public_key}"
  }
}

data "template_cloudinit_config" "cloud_init" {
  base64_encode = false
  gzip          = false

  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.deploy_user.rendered}"
    merge_type   = "list(append)+dict(recurse_array)+str()"
  }

  part {
    content_type = "text/cloud-config"
    content      = "${file("${path.module}/files/cloud-init/upgrade_packages.cfg")}"
  }

  part {
    content_type = "text/cloud-config"
    content      = "${file("${path.module}/files/cloud-init/ansible_target.cfg")}"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "instance" {
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "${var.instance_size}"
  key_name               = "${var.key_name}"
  vpc_security_group_ids = ["${aws_security_group.evanfarrell.id}"]
  user_data              = "${data.template_cloudinit_config.cloud_init.rendered}"
  subnet_id              = "${aws_subnet.public.id}"

  tags = "${merge(
       local.common_tags,
       map("Name", "Evan Farrell Tech Challenge")
     )}"
}

output "instance_public_ip" {
  value = "${aws_instance.instance.public_ip}"
}
