package main

import (
	"bufio"
	"net/http"
	"os/user"

	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"
)

var (
	aws_profile           string
	environment           string
	terraform_path        string
	ssh_public_key_path   string
	aws_secret_access_key string
	aws_access_key        string
)

type Terraform struct {
	Path        string
	Environment string

	AwsProfile         string
	AwsAccessKey       string
	AwsSecretAccessKey string
	SshKey             string
	MyIP               string
}

func main() {
	tf := New()
	if len(os.Args) > 1 {
		if os.Args[1] == "destroy" {
			tf.Destroy()
			return
		}
	}
	tf.Init()
	tf.Plan()
	reader := bufio.NewReader(os.Stdin)
	var text string
	var err error
	for i := 1; i <= 5; i++ {
		fmt.Print("Apply terraform (yes/no): ")
		text, err = reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		text = strings.ToLower(strings.TrimSpace(text))
		if text != "yes" && text != "no" {
			fmt.Println("input must be one of 'yes' or 'no'.")
		} else {
			break
		}
	}
	if text != "yes" && text != "no" {
		log.Fatal("incorrect input")
	}
	if text == "yes" {
		tf.Apply()
	}

}

func (tf *Terraform) Plan() {
	cmd := exec.Command("terraform", "plan", "-out", "plan.plan", "-var", fmt.Sprintf("ssh_public_key=%v", tf.SshKey), "-var", fmt.Sprintf("my_ip=%v", tf.MyIP))
	cmd.Env = append(os.Environ(),
		"TF_IN_AUTOMATION=true",
		fmt.Sprintf("AWS_PROFILE=%v", tf.AwsProfile),
		fmt.Sprintf("AWS_ACCESS_KEY=%v", tf.AwsAccessKey),
		fmt.Sprintf("AWS_SECRET_ACCESS_KEY=%v", tf.AwsSecretAccessKey),
	)
	cmd.Dir = tf.Path
	out, err := cmd.Output()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(out))
}
func (tf *Terraform) Init() {
	cmd := exec.Command("./init.sh")
	cmd.Dir = tf.Path
	_, err := cmd.Output()
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Init Successful")
}
func (tf *Terraform) Apply() {
	cmd := exec.Command("terraform", "apply", "plan.plan")
	cmd.Env = append(os.Environ(),
		"TF_IN_AUTOMATION=true",
		fmt.Sprintf("AWS_PROFILE=%v", tf.AwsProfile),
		fmt.Sprintf("AWS_ACCESS_KEY=%v", tf.AwsAccessKey),
		fmt.Sprintf("AWS_SECRET_ACCESS_KEY=%v", tf.AwsSecretAccessKey),
	)
	cmd.Dir = tf.Path
	out, err := cmd.Output()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(out))
}
func (tf *Terraform) Destroy() {
	cmd := exec.Command("terraform", "destroy", "-force", "-var", fmt.Sprintf("my_ip=%v", tf.MyIP), "-var", "ssh_public_key=1")
	cmd.Env = append(os.Environ(),
		"TF_IN_AUTOMATION=true",
		fmt.Sprintf("AWS_PROFILE=%v", tf.AwsProfile),
		fmt.Sprintf("AWS_ACCESS_KEY=%v", tf.AwsAccessKey),
		fmt.Sprintf("AWS_SECRET_ACCESS_KEY=%v", tf.AwsSecretAccessKey),
	)
	cmd.Dir = tf.Path
	out, err := cmd.Output()
	if err != nil {
		log.Println(string(out))
		log.Fatal(err)
	}
	log.Println(string(out))
}

func New() *Terraform {
	dat, err := ioutil.ReadFile(ssh_public_key_path)
	if err != nil {
		log.Fatal(err)
	}
	// Get Ip for security group
	url := "https://api.ipify.org?format=text" // we are using a pulib IP API, we're using ipify here, below are some others
	// https://www.ipify.org
	// http://myexternalip.com
	// http://api.ident.me
	// http://whatismyipaddress.com/api
	fmt.Printf("Getting IP address from  ipify ...\n")
	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	ip, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Printf("My IP is:%s\n", ip)

	return &Terraform{
		SshKey:             string(dat),
		Path:               terraform_path,
		Environment:        environment,
		AwsProfile:         aws_profile,
		AwsSecretAccessKey: aws_secret_access_key,

		AwsAccessKey: aws_access_key,
		MyIP:         string(ip),
	}

}

func init() {
	environment = os.Getenv("ENVIRONMENT")
	if environment == "" {
		environment = "qa"
	}
	aws_secret_access_key = os.Getenv("AWS_SECRET_ACCESS_KEY")
	aws_access_key = os.Getenv("AWS_ACCESS_KEY")
	aws_profile = os.Getenv("AWS_PROFILE")
	if aws_profile == "" && aws_access_key == "" {
		aws_profile = "default"
	}

	ssh_public_key_path = os.Getenv("SSH_PUBLIC_KEY_PATH")
	if ssh_public_key_path == "" {
		usr, err := user.Current()
		if err != nil {
			log.Fatal(err)
		}
		ssh_public_key_path = fmt.Sprintf("%v/.ssh/id_rsa.pub", usr.HomeDir)
	}

	terraform_path = os.Getenv("TF_RUN_PATH")
	if terraform_path == "" {
		terraform_path = fmt.Sprintf("tf/%v", environment)
	}
}
